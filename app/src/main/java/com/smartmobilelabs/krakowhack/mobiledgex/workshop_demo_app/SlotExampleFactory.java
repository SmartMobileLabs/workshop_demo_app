package com.smartmobilelabs.krakowhack.mobiledgex.workshop_demo_app;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.smartmobilelabs.evo.android.ApplicationInterfaces.IFactoryViewsManager;
import com.smartmobilelabs.evo.android.playersdk.player.Slot.Slot;
import com.smartmobilelabs.evo.android.playersdk.player.view.DefaultHudView;

public class SlotExampleFactory implements IFactoryViewsManager {

    private Context mContext;
    private Activity mActivity;

    SlotExampleFactory(Context context, Activity activity) {
        super();
        this.mContext = context;
        this.mActivity = activity;
    }

    public View createHudView(Slot slot) {
        return new DefaultHudView(mContext, slot); // TODO: create hud view
    }

    public View createDataOverlayView(Slot slot) {
        return new OverlayViewExample(mContext, mActivity); // TODO: create overlay view
    }
}
